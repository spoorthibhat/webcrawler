package com.Spider.core;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.LinkedList;
import java.util.Queue;

import com.URLSupplier.URLSupply;


public class MainThread {

	private Queue<String> queueA;
	private Queue<String> queueB;
	private Queue<String> queueC;

	public MainThread(){

		queueA = new LinkedList<>();
		queueB = new LinkedList<>();
		queueC = new LinkedList<>();
	}

	/**
	 * This method accepts a list of URLs separated by comma(,) 
	 * and add them to the URL supply.
	 * @param listOfURLs - the seed URLs.
	 */
	public void feedSeedUrls(String listOfURLs){

		try{
			String[] urls = listOfURLs.split(",");
			for(String seedURL : urls){
				URLSupply.addURL(seedURL);
			}
		}catch(UnsupportedEncodingException e){
			System.err.println("Error during feeding the seed URLs to the URL Supply : " + e);
		} catch (MalformedURLException e) {
			System.err.println("Error during feeding the seed URLs to the URL Supply : " + e);
		}
	}
	public void beginCrawl() throws InterruptedException, IOException {

		long startTime = System.nanoTime();

		MainThread mt = new MainThread();
		//mt.feedSeedUrls("https://en.wikipedia.org/wiki/Computer,https://www.tutorialspoint.com/java/lang/java_lang_stringbuilder.htm,https://www.geeksforgeeks.org/synchronized-in-java/,https://en.wikipedia.org/wiki/J._K._Rowling");
		Crawler crawl1 = new Crawler(mt.queueA, mt.queueB, mt.queueC, "Thread 1");
		Crawler crawl2 = new Crawler(mt.queueA, mt.queueB, mt.queueC, "Thread 2");
		Crawler crawl3 = new Crawler(mt.queueA, mt.queueB, mt.queueC, "Thread 3");
		Crawler crawl4 = new Crawler(mt.queueA, mt.queueB, mt.queueC, "Thread 4");
		Crawler crawl5 = new Crawler(mt.queueA, mt.queueB, mt.queueC, "Thread 5");
		crawl1.start();
		crawl2.start();
		crawl3.start();
		crawl4.start();
		crawl5.start();
		crawl1.join();
		crawl2.join();
		crawl3.join();
		crawl4.join();
		crawl5.join();
		long endTime   = System.nanoTime();
		long totalTime = endTime - startTime;
		System.out.println(totalTime/1000000000);

        // 4 threads would process each url in 1s. 5 threads takes less than a sec.
	}

}
