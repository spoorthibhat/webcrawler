package com.Spider.core;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Random;

import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import com.URLSpillter.HashCodeGenerator;
import com.URLSpillter.URLSplit;
import com.URLSupplier.URLSupply;
import com.berkeleyDB.DatabaseInstances;
import com.berkeleyDB.PageContentStorage;
import com.parser.CustomHTTPClient;
import com.parser.Parser;
import com.publisher.PublishingThread;
import com.validators.ContentValidator;
import com.validators.CrawlPreConditionValidator;

public class Crawler extends Thread{

	private Queue<String> queueA;
	private Queue<String> queueB;
	private Queue<String> queueC;
	private CrawlPreConditionValidator preconditionValidator;
	private String threadName;
	private HashCodeGenerator hashCodeGenerator;
	private ContentValidator validateContent;
	private URLSplit split;
	private static PublishingThread publishingThread;

	public Crawler(Queue<String> queueA,Queue<String> queueB,Queue<String> queueC,String threadName) throws MalformedURLException, UnsupportedEncodingException{

		this.queueA = queueA;
		this.queueB = queueB;
		this.queueC = queueC;

		this.threadName = threadName;
		preconditionValidator = new CrawlPreConditionValidator();
		hashCodeGenerator = new HashCodeGenerator();
		validateContent = new ContentValidator();
		split = new URLSplit();
	}

	public void run(){

		try {
			for(int i = 0 ; i <= 49; i++){
				String extractedUrl = URLSupply.fetchURL();

				System.out.println(threadName + " is processing url - " + extractedUrl + "(" + i + ")");
				if(preconditionValidator.isAllowedToCrawl(extractedUrl)){
					CustomHTTPClient client = new CustomHTTPClient();
					HttpResponse response = client.getHttpResponse(extractedUrl);
					if(response == null){
						continue;
					}
					Thread.sleep(randomSleepTimeGenerator());
					if(validateContent.isValid(response)){
						// webpage dao
						storePageContents(extractedUrl,response);
						//extract links
						Parser parser = new Parser(extractedUrl);
						ArrayList<String> links = parser.extractLinks();
						for(String foundLink: links){

							if(!foundLink.isEmpty()){
								String hashCode = hashCodeGenerator.generateHashCode(foundLink);
								split.splitUrl(foundLink, hashCode);

								// TODO : publish every few minutes only
								if(publishingThread == null){
									publishingThread = new PublishingThread();
									publishingThread.start();
								}
							}
						}
					}
				}

			}
			// TODO : check what happens when the cache is full.
			DatabaseInstances.closePageContentsDBInstance();
			DatabaseInstances.closeRobotsDBInstance();
			DatabaseInstances.closeUrlsDBInstance();
		}catch (Exception e) {
			System.err.println(e);
		}

	}


	/**
	 * 
	 * @param hashCode - The hashcode of the link found in the page parsed.
	 * @param extractedLink -  The link found.
	 * @return - Returns the system where the urls have to pushed into.
	 * @throws UnsupportedEncodingException 
	 * @throws MalformedURLException 
	 */
//	private void splitUrls(String hashCode,String extractedLink) throws MalformedURLException, UnsupportedEncodingException{
//		URLSplit urlsplitter = new URLSplit();
//		String toBeSentSystem = urlsplitter.getSystemToPushUrls(urlsplitter.getSplitNumber(hashCode));
//		if(toBeSentSystem.equals("A")){
//			queueA.add(extractedLink);
//		}else if(toBeSentSystem.equals("B")){
//			queueB.add(extractedLink);
//		}else if(toBeSentSystem.equals("C")){
//			queueC.add(extractedLink);
//		}else{
//			URLSupply.addURL(extractedLink);
//		}
//
//
//	}

	/**
	 * Helper method to store the retrieved page contents to database.
	 * @param url - URL for which the contents are to be stored.
	 * @param response - Http response.
	 * @throws IOException
	 */  //TODO - DatabaseLayer should be synchronized and not here.
	private static synchronized void storePageContents(String url,HttpResponse response) throws IOException{

		PageContentStorage contentsDB = DatabaseInstances.getPageContentStorageInstance();
		String content = EntityUtils.toString(response.getEntity());
		contentsDB.putIntoPageContentDB(url, content);

	}

	private int randomSleepTimeGenerator(){
		Random rand = new Random();
		return (rand.nextInt(3) + 2) * 1000;
	}

}
