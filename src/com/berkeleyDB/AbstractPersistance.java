package com.berkeleyDB;

import java.util.ArrayList;
import java.util.Queue;

import com.sleepycat.je.Environment;

public abstract class AbstractPersistance {
	public static final String BERKLEY_DB_PATH = "/BerkleyDB/TestDB/Another";

	public static final String PageStorageDBName = "PageContentDB7";
	public static final String RobotsDBName = "RobotsDB7";
	public static final String UrlsDB = "URLsDB7";


	public static Environment env;
	
	public static ArrayList<String> theHosts = new ArrayList<>();
	public static ArrayList<Queue<String>> publishingQueue = new ArrayList<>();
}
