package com.berkeleyDB;

import java.io.File;
import java.io.UnsupportedEncodingException;

import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.LockMode;
import com.sleepycat.je.OperationStatus;

public class RobotsContentStorage extends AbstractPersistance {

	private Database robotsDB; // database
	
	/**
	 * This is a constructor that initializes the environment
	 * and sets up the database to store the disallowed URLs of the robots.txt file
	 * corresponding to host name of the link picked up for crawling.
	 */
	public RobotsContentStorage(){

		EnvironmentConfig envConfig = new EnvironmentConfig();
		envConfig.setAllowCreate(true);
		env = new Environment(new File(BERKLEY_DB_PATH),envConfig);
		
		DatabaseConfig dbconf = new DatabaseConfig();
		dbconf.setAllowCreate(true);
		
		robotsDB = env.openDatabase(null, RobotsDBName, dbconf);
		
	}

	/**
	 * This method is used to put the key-value pair into the database.
	 * @param key - key value(URL)
	 * @param value - data value(hostname)
	 * @throws UnsupportedEncodingException - Throws exception if there is problem with encoding.
	 */
	public void putIntoRobotsDb(String key, String value) throws UnsupportedEncodingException{

		DatabaseEntry keyEntry = new DatabaseEntry(key.getBytes("UTF-8"));
		DatabaseEntry valueEntry = new DatabaseEntry(value.getBytes("UTF-8"));

		robotsDB.put(null, keyEntry, valueEntry);
	}

	/**
	 * This method is used to get the data from the database corresponding to the given key.
	 * @param key - key value for which the data has to be fetched.
	 * @return - returns the fetched value.
	 * @throws UnsupportedEncodingException - Throws exception if there is problem with encoding.
	 */
	public String getFromRobotsDb(String key) throws UnsupportedEncodingException{

		String value = "";

		DatabaseEntry keyEntry = new DatabaseEntry(key.getBytes("UTF-8"));
		DatabaseEntry valueEntry = new DatabaseEntry();

		if (robotsDB.get(null, keyEntry, valueEntry, LockMode.DEFAULT) == OperationStatus.SUCCESS){
			byte[] theValue = valueEntry.getData();
			value = new String(theValue, "UTF-8");

		}
		return value;
	}

	/**
	 * This method closes the database, RobotsDB.
	 */
	public void closeRobotsDB(){

		try{

			if(robotsDB != null){
				robotsDB.close();
			}

			if(env != null){
				env.close();
			}
			
			DatabaseInstances.closeRobotsDBInstance();
		}catch(DatabaseException e){
			System.out.println("Problem while closing databse : " + e.getMessage());
		}
	}

	public static void main(String[] args) throws UnsupportedEncodingException {
		// TODO Auto-generated method stub

		//test
		RobotsContentStorage storage = new RobotsContentStorage();
		//		storage.putIntoRobotsDb("t-shirt", "cloth");
		//	    storage.putIntoRobotsDb("barbie", "doll");

		System.out.println("coderanch.com : " +storage.getFromRobotsDb("coderanch.com"));

		storage.closeRobotsDB();
	}

}
