package com.berkeleyDB;

public class DatabaseInstances {

	private static PageContentStorage pageContentsStorage;
	private static RobotsContentStorage robotsStorage;
	private static URLsStorage urlsStorage;

	private DatabaseInstances() {

	}

	/**
	 * Gets the instance of PageContentStorage object.
	 * @return - the PageContentStorage object.
	 */
	public static PageContentStorage getPageContentStorageInstance(){

		if(pageContentsStorage == null){
			pageContentsStorage = new PageContentStorage();
		}

		return pageContentsStorage;
	}
	
	/**
	 * Closes PageContentStorage object.
	 */
	public static void closePageContentsDBInstance() {
		pageContentsStorage = null;
	}

	/**
	 * Closes RobotsContentStorage object.
	 */
	public static void closeRobotsDBInstance() {
		robotsStorage = null;
	}
	
	/**
	 * Closes URLsStorage object.
	 */
	public static void closeUrlsDBInstance() {
		urlsStorage = null;
	}
	
	/**
	 * Gets the instance of RobotsContentStorage object.
	 * @return - the RobotsContentStorage object.
	 */
	public static RobotsContentStorage getRobotsStorageInstance(){

		if(robotsStorage == null){
			robotsStorage = new RobotsContentStorage();
		}

		return robotsStorage;
	}

	/**
	 * Gets the instance of URLsStorage object.
	 * @return - the URLsStorage object.
	 */
	public static URLsStorage getURLsStorageInstance(){

		if(urlsStorage == null){
			urlsStorage = new URLsStorage();
		}

		return urlsStorage;
	}

}
