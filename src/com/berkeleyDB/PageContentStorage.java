
package com.berkeleyDB;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.LockMode;
import com.sleepycat.je.OperationStatus;

public class PageContentStorage extends AbstractPersistance{
	
	public Database pageContentDB;
	/**
	 * This is a constructor that initializes the environment
	 * and sets up the database to store the contents of the pages 
	 * corresponding to a link picked up for crawling.
	 */
	public PageContentStorage(){
		
		EnvironmentConfig envConfig = new EnvironmentConfig();
		envConfig.setAllowCreate(true);
		
		env = new Environment(new File(BERKLEY_DB_PATH), envConfig);
		DatabaseConfig dbConfig = new DatabaseConfig();
		dbConfig.setAllowCreate(true);
		
		pageContentDB = env.openDatabase(null, PageStorageDBName, dbConfig);
	}
	
	/**
	 * This method is used to put the key-value pair into the database.
	 * @param key - key value.
	 * @param value - data value.
	 * @throws UnsupportedEncodingException - Throws exception if there is problem with encoding.
	 */
	public void putIntoPageContentDB(String key, String value) throws UnsupportedEncodingException{
		
		DatabaseEntry keyEntry = new DatabaseEntry(key.getBytes("UTF-8"));
		DatabaseEntry valueEntry = new DatabaseEntry(value.getBytes("UTF-8"));
		
		pageContentDB.put(null, keyEntry, valueEntry);
		
	}
	
	/**
	 * This method is used to get the data from the database corresponding to the given key.
	 * @param key - key value for which the data has to be fetched.
	 * @return - returns the fetched value.
	 * @throws UnsupportedEncodingException - Throws exception if there is problem with encoding.
	 */
	public String getPageContentDB(String key) throws UnsupportedEncodingException{
		
		String value = "";
		
		DatabaseEntry keyEntry = new DatabaseEntry(key.getBytes("UTF-8"));
		DatabaseEntry valueEntry = new DatabaseEntry();
		
		if (pageContentDB.get(null, keyEntry, valueEntry, LockMode.DEFAULT) == OperationStatus.SUCCESS){
			byte[] theValue = valueEntry.getData();
			value = new String(theValue, "UTF-8");
			
		}
		return value;
	}
	
	/**
	 * This method closes the database, PageContentDB.
	 */
	public void closeContentStorage(){
		
		try{
			
			if(pageContentDB != null){
				pageContentDB.close();
			}
			
			if(env != null){
				env.close();
			}
			
			DatabaseInstances.closePageContentsDBInstance();
		}catch(DatabaseException e){
			System.out.println("Problem while closing databse : " + e);
		}
	}
	
	public static void main(String[] args) throws IOException{
		
		String url = "https://www.tutorialspoint.com/java/lang/java_lang_boolean.htm";
		PageContentStorage ps = new PageContentStorage();
		
		System.out.println(ps.getPageContentDB(url));
	}
}
