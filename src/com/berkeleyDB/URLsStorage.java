package com.berkeleyDB;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import com.sleepycat.je.Cursor;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.LockMode;
import com.sleepycat.je.OperationStatus;

public class URLsStorage extends AbstractPersistance {

	private Database urlsDB; // database
	private EnvironmentConfig envConfig;
	
	/**
	 * Constructor that initializes the environment and sets up the 
	 * database to store the list of URLs to be crawled.
	 */
	public URLsStorage(){
		
		envConfig = new EnvironmentConfig();
		envConfig.setAllowCreate(true);
		//envConfig.setCacheSize(2*1024*1024);
		env = new Environment(new File(BERKLEY_DB_PATH),envConfig);
		DatabaseConfig dbconf = new DatabaseConfig();
		dbconf.setAllowCreate(true);
		
		urlsDB = env.openDatabase(null, UrlsDB, dbconf);
	}
	
	/**
	 * This method is used to put the key-value pair into the database.
	 * @param key - key value.
	 * @param value - data value.
	 * @throws UnsupportedEncodingException - Throws exception if there is problem with encoding.
	 */
	public void putIntoUrlsDB(String key, String value) throws UnsupportedEncodingException{
		
		DatabaseEntry keyEntry = new DatabaseEntry(key.getBytes("UTF-8"));
		DatabaseEntry valueEntry = new DatabaseEntry(value.getBytes("UTF-8"));
		
		urlsDB.put(null, keyEntry, valueEntry);
		//System.out.println(key + " added to" + urlsDB.getDatabaseName());
	}
	
	/**
	 * This method is used to get the data from the database corresponding to the given key.
	 * @param key - key value for which the data has to be fetched.
	 * @return - returns the fetched value.
	 * @throws UnsupportedEncodingException - Throws exception if there is problem with encoding.
	 */
	public String getFromUrlsDB(String key) throws UnsupportedEncodingException{
		
		String value = "";
		
		DatabaseEntry keyEntry = new DatabaseEntry(key.getBytes("UTF-8"));
		DatabaseEntry valueEntry = new DatabaseEntry();
		
		if (urlsDB.get(null, keyEntry, valueEntry, LockMode.DEFAULT) == OperationStatus.SUCCESS){
			byte[] theValue = valueEntry.getData();
			value = new String(theValue, "UTF-8");
			
		}
		return value;
	}
	
	/**
	 * Deletes the data corresponding to the given key.
	 * @param key - key value
	 * @throws UnsupportedEncodingException - Throws exception if there is problem with encoding.
	 */
	public void deleteFromUrlsDB(String key) throws UnsupportedEncodingException{
		
		DatabaseEntry keyValue = new DatabaseEntry(key.getBytes("UTF-8"));
		urlsDB.delete(null, keyValue);
	}
	
	/**
	 * Gets the next URL that has to be picked up for crawling.
	 * @return - Returns the URL string.
	 * @throws UnsupportedEncodingException - Throws exception if there is problem with encoding.
	 */
	public String getNextURL() throws UnsupportedEncodingException{
		
		Cursor urlCursor = null;
		String key = "";
		try{
			urlCursor = urlsDB.openCursor(null, null);
			DatabaseEntry foundKey = new DatabaseEntry();
			DatabaseEntry foundValue = new DatabaseEntry();
			
			if(urlCursor.getNext(foundKey, foundValue, LockMode.DEFAULT) == OperationStatus.SUCCESS){
				key = new String(foundKey.getData(),"UTF-8");
			}
		}catch(DatabaseException e){
			System.out.println("Error reading from DB : " + e.getMessage());
		}finally{
			
			try {
		        if (urlCursor != null) {
		            urlCursor.close();
		        }
		    } catch(DatabaseException dbe) {
		        System.err.println("Error closing cursor: " + dbe.toString());
		    }
		}
		
		return key;
	}
	
	/**
	 * This method closes the database, RobotsDB.
	 */
	public void closeUrlsDB(){
		
		try{
			
			if(urlsDB != null){
				urlsDB.close();
			}
			
			if(env != null){
				env.close();
			}
			
			DatabaseInstances.closeUrlsDBInstance();
		}catch(DatabaseException e){
			System.out.println("Problem while closing databse : " + e.getMessage());
		}
	}
	
	public static void main(String[] args) throws UnsupportedEncodingException, MalformedURLException {
		//test
		
		
//		for(int i = 0; i <= 5; i++){
//		System.out.println(storage.getNextURL());
//
//		}
		
//		URLSupply sup = new URLSupply();
//		sup.addURL("https://www.google.com/chocolate");
//		sup.addURL("https://www.google.com/cake");
//		sup.addURL("https://www.google.com/android");
//		sup.addURL("https://www.amaozon.com/seattle");
//		
		URLsStorage storage = new URLsStorage();
		//storage.putIntoUrlsDB("Amazon", "Alexa");
//		storage.putIntoUrlsDB("Google", "Android");
//		storage.putIntoUrlsDB("Netflix", "StarngerThings");
//		
		//storage.deleteFromUrlsDB("Amazon");
		storage.putIntoUrlsDB("https://www.amazon", "Amazon");
		storage.putIntoUrlsDB("https://www.google", "google");
		storage.putIntoUrlsDB("https://www.netflix", "netflix");
		//System.out.println(storage.getNextURL());
		//storage.closeUrlsDB();
		System.out.println(storage.getNextURL());
		System.out.println(storage.envConfig.getCacheSize());
		//Fetched https://en.wikipedia.org/wiki/Computer
		// Fetched https://en.wikipedia.org/wiki/Portal:Biography

	}

}
