package com.publisher;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

public class URLPublisher {

	private static final int QUEUELIMIT= 10;
	
	public String getURLsFromQueue(Queue<String> overflowingQueue){
		
		String urls = "";
		for(int i = 0; i < QUEUELIMIT; i++){
			urls += overflowingQueue.remove() + ",";
		}
		return urls;
	}
	
	public void publishURLs(String urlList,String toBePublishedSystem) throws IOException{
		
		String postURL = getPostURL(toBePublishedSystem);
	    HttpPost post = new HttpPost(postURL);

	    List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("URLs", urlList));

	    UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, "UTF-8");
	    post.setEntity(entity);

	    HttpClient client = HttpClientBuilder.create().build();
	    HttpResponse responsePOST = client.execute(post);
	    
	    String responseString = new BasicResponseHandler().handleResponse(responsePOST);
	    System.out.println(responseString);
	}
	
	private String getPostURL(String toBePublishedSystem){
		
		
		if(toBePublishedSystem.equals("A")){
			return "http://TOBECHANGED:8081/WebCrawler/CollectorServlet";
		}else if(toBePublishedSystem.equals("B")){
			return "http://TOBECHANGED:8081/WebCrawler/CollectorServlet";
		}else if (toBePublishedSystem.equals("C")){
			return "http://TOBECHANGED:8081/WebCrawler/CollectorServlet";
		}else{
			return "http://localhost:8081/WebCrawler/CollectorServlet";
		}
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
