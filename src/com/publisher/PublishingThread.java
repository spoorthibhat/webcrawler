package com.publisher;

import java.io.IOException;
import java.util.Queue;

public class PublishingThread extends Thread{

	private URLPublisher publisher;
	private Queue<String> queueA;
	private Queue<String> queueB;
	private Queue<String> queueC;
	private static final int LIMIT = 5;

	public PublishingThread(Queue<String> queueA,Queue<String> queueB,Queue<String> queueC){
		publisher = new URLPublisher();
		this.queueA = queueA;
		this.queueB = queueB;
		this.queueC = queueC;

	}
	public void run(){

		while(thereIsDataToPublish()){
			try{
				if(queueA.size() > LIMIT){
					publisher.publishURLs(publisher.getURLsFromQueue(queueA,LIMIT), "A");
				}
				if(queueB.size() > LIMIT){
					publisher.publishURLs(publisher.getURLsFromQueue(queueB,LIMIT), "B");
				}
				if(queueC.size() > LIMIT){
					publisher.publishURLs(publisher.getURLsFromQueue(queueC,LIMIT), "C");
				}
			}catch(IOException e){
				System.err.println(e.getMessage());
			}
		}
	}


	private boolean thereIsDataToPublish(){

		return(!queueA.isEmpty() || !queueB.isEmpty() || !queueC.isEmpty());
	}
}
