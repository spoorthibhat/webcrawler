package com.parser;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.berkeleyDB.PageContentStorage;

public class Parser {

	private String urlUnderConsideration;
	
	public Parser(String urlUnderConsideration){
		
		this.urlUnderConsideration = urlUnderConsideration;
	}
	public Document getHtmlDoc(HttpResponse response) throws IOException{
		
		InputStream in = response.getEntity().getContent();
		Document doc = Jsoup.parse(in, null, urlUnderConsideration);
		in.close();
		
		return doc;
	}
	
	public Elements extractLinks(Document inputDoc){
		
		return inputDoc.select("a[href]");
	}
	
	public void storePageContents(Document htmlDocument) throws UnsupportedEncodingException{
		
		PageContentStorage contentsDB = new PageContentStorage();
		contentsDB.putIntoPageStorage(urlUnderConsideration, htmlDocument.toString());
		contentsDB.closeContentStorage();
		
	}
	public static void main(String[] args) throws IOException {
		//Test
		HTTPClient cl = new HTTPClient();
     	String url = "https://www.tutorialspoint.com/java/io/inputstream_read.htm";
		HttpResponse res = cl.getHttpResponse(url);
		
		Parser parse = new Parser(url);
		Elements links = parse.extractLinks(parse.getHtmlDoc(res));
		for(Element link: links){
			System.out.println(link.attr("abs:href")); // fetching absolute url
		}

		
	}

}
