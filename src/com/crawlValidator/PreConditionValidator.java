package com.crawlValidator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;


public class PreConditionValidator {


	public boolean validator(String inputUrl, Map<String, ArrayList<String>> disallowedUrls) throws IOException{

		URL url = new URL(inputUrl);
		if(!disallowedUrls.containsKey(url.getHost())){
			String urlForRobot = url.getProtocol() + "://" + url.getHost() + "/robots.txt";
			fetchContentsOfRobotstxt(urlForRobot,disallowedUrls);
		}
		return validateIfAllowed(disallowedUrls.get(url.getHost()), url.getPath());
	}

	private boolean validateIfAllowed(ArrayList<String> urlsList, String urlPath){

		if(!urlsList.isEmpty()){
			for(String disallowedUrl : urlsList){
				disallowedUrl = disallowedUrl.replaceAll("\\*", ".*");
				if(urlPath.matches(disallowedUrl)){
					return false;
				}
			}
		}
		return true;
	}

	private void fetchContentsOfRobotstxt(String robotsUrl,Map<String, ArrayList<String>> disallowedUrls) throws IOException{

		URL urlForRobot = new URL(robotsUrl);
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(robotsUrl); 
		HttpResponse response = client.execute(request);

		ArrayList<String> disallowedUrlsList = new ArrayList<String>();
		if(response.getStatusLine().getStatusCode() == 200){

			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String line = reader.readLine();


			while(!line.equals("User-agent: *") && line != null){ // keep searching until you find user-agent as *
				line = reader.readLine();
			}

			if(line.equals("User-agent: *")){

				while((line = reader.readLine())!= null){
					if(line.startsWith("Disallow:")){
						String[] lineContents = line.trim().split(" ");
						disallowedUrlsList.add(lineContents[1]);
					}
				}
			}
		}

		disallowedUrls.put( urlForRobot.getHost(), disallowedUrlsList);
	}

	public static void main(String[] args) throws IOException {

		// Test
		Map<String, ArrayList<String>> disallowed = new HashMap<String, ArrayList<String>>();
		String url = "https://www.tutorialspoint.com/videos/something";

		PreConditionValidator pv = new PreConditionValidator();
		System.out.println(pv.validator(url, disallowed));

	}

}
