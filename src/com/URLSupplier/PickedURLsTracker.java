package com.URLSupplier;

import java.util.HashSet;

public class PickedURLsTracker {

	private static HashSet<String> pickedURLsTracker;
	
	private PickedURLsTracker() {
	}
	
	public static HashSet<String> getTrackerInstance(){
		if(pickedURLsTracker == null || pickedURLsTracker.size() > 20){
			pickedURLsTracker = new HashSet<String>();
		}
		
		return pickedURLsTracker;
	}

}
