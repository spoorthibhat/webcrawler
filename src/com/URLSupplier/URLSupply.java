package com.URLSupplier;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;

import com.berkeleyDB.DatabaseInstances;
import com.berkeleyDB.PageContentStorage;
import com.berkeleyDB.URLsStorage;

public class URLSupply {

	/**
	 * Method to add URL. key - URL, value - its host name
	 * @param url - URL to be stored.
	 * @throws MalformedURLException - Throws exception if URL is 
	 * not formed properly.
	 * @throws UnsupportedEncodingException - Throws exception if 
	 * there is problem with encoding.
	 */
	public static synchronized void addURL(String url) throws MalformedURLException, UnsupportedEncodingException{

		URL inpUrl = new URL(url);
		URLsStorage supplyDB = DatabaseInstances.getURLsStorageInstance();
		supplyDB.putIntoUrlsDB(url, inpUrl.getHost());

	}

	/**
	 * Method to fetch URL from the URL Supply.
	 * @return - returns the fetched URL.
	 * @throws UnsupportedEncodingException - throws exception if 
	 * there was an error during encoding.
	 */
	public static synchronized String fetchURL() throws UnsupportedEncodingException{

		URLsStorage supplyDB = DatabaseInstances.getURLsStorageInstance();
		String fetchedURL;
		PageContentStorage pageContentsStorage = DatabaseInstances.getPageContentStorageInstance();
		HashSet<String> urlsTracker = PickedURLsTracker.getTrackerInstance();
		do{
			fetchedURL = supplyDB.getNextURL();
		}while(pageContentsStorage.wasCrawledBefore(fetchedURL) || urlsTracker.contains(fetchedURL));

		urlsTracker.add(fetchedURL);
		return fetchedURL;
	}

}
