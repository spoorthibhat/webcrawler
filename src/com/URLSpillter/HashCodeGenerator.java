package com.URLSpillter;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class HashCodeGenerator {

	public String generateHashCode(String inputURL) throws Exception{
		// get the hash code for the host name of the URL
		URL url = new URL(inputURL);
		
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] byteData = digest.digest(url.getHost().getBytes(StandardCharsets.UTF_8));
		
		StringBuffer hexString = new StringBuffer();
    	for (int i=0;i<byteData.length;i++) {
    		String hex=Integer.toHexString(0xff & byteData[i]);
   	     	if(hex.length()==1) hexString.append('0');
   	     	hexString.append(hex);
    	}
		
    	return hexString.toString();
	}
	
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		HashCodeGenerator hc = new HashCodeGenerator();
		System.out.println(hc.generateHashCode("amazon"));
	}

}
