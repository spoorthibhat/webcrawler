package com.validators;

import java.io.IOException;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;


public class ContentValidator {

	public boolean validateContentType(HttpResponse response){
		
		if(response.getStatusLine().getStatusCode() == 200){
			
			String contentType = getContentType(response);
			if(contentType.equals("text/html")){
				return true;
			}
		}
		return false;
	}
	
	private String getContentType(HttpResponse response){
		HttpEntity entity = response.getEntity();
		ContentType contentType = null;
		if(entity != null){
			contentType = ContentType.get(entity);
		}
		
		return contentType.getMimeType();
		
	}
	public static void main(String[] args) throws IOException{
		
		//Test
		String url = "https://en.wikipedia.org/wiki/Science";
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet req = new HttpGet(url);
		HttpResponse res = client.execute(req);
		
		ContentValidator val = new ContentValidator();
		System.out.println(val.validateContentType(res));
	}
}
