package com.validators;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.entity.ContentType;

import com.berkeleyDB.DatabaseInstances;
import com.berkeleyDB.RobotsContentStorage;
import com.parser.CustomHTTPClient;
import com.sleepycat.je.DatabaseConfig;


public class CrawlPreConditionValidator {

	private RobotsContentStorage robotStorage;
	/**
	 * Method that checks if the fetched URL is allowed 
	 * to crawl or not according to the /robots.txt.
	 * @param inputUrl - fetched URL for crawling.
	 * @return - true if URL is allowed to be crawled else false.
	 * @throws IOException
	 */
	public boolean isAllowedToCrawl(String inputUrl) throws IOException{

		URL url = new URL(inputUrl);
		robotStorage = DatabaseInstances.getRobotsStorageInstance();
		
			
			String fetchedRobotsDisalllowedUrls = robotStorage.getFromRobotsDb(url.getHost());
			
			if(fetchedRobotsDisalllowedUrls.equals("")){
				String urlForRobot = url.getProtocol() + "://" + url.getHost() + "/robots.txt";
				fetchedRobotsDisalllowedUrls = fetchContentsOfRobotstxt(urlForRobot,robotStorage);
			}

			//robotStorage.closeRobotsDB();
			return validateIfAllowed(fetchedRobotsDisalllowedUrls, url.getPath());
		
		
	}

	/**
	 * A private helper method that helps in validating.
	 * @param urlsList - list of disallowed URLs as specified in the /robots.txt 
	 * in the form of a string.
	 * @param urlPath - URL's path.
	 * @return - returns true if validated to be allowed or else returns false
	 */
	private boolean validateIfAllowed(String urlsList, String urlPath){

		if(!urlsList.equals("") || urlsList != null){ // todo: check for urlsList = ""
			String[] disallowedUrls = urlsList.split(",");
			for(int i = 0; i < disallowedUrls.length; i++){
				String disallowedUrl = disallowedUrls[i];
				disallowedUrl = disallowedUrl.replaceAll("\\*", ".*");
				if(urlPath.matches(disallowedUrl)){
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Private helper method to fetch the contents of /robots.txt and 
	 * loads it into RobotsDB.
	 * @param robotsUrl - URL to hit /robots.txt.
	 * @param robotStorage - RobotsContentStorage object.
	 * @return - fetched disallowed URLs.
	 * @throws IOException
	 */
	private static synchronized String fetchContentsOfRobotstxt(String robotsUrl,RobotsContentStorage robotStorage ) throws IOException{

		URL urlForRobot = new URL(robotsUrl);
		CustomHTTPClient client = new CustomHTTPClient();
		HttpResponse response = client.getHttpResponse(robotsUrl);

		String disallowedUrlsList = "";
		if(response.getStatusLine().getStatusCode() == 200){

			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String line = reader.readLine();

            
			while(!line.equals("User-agent: *") && line != null){ // keep searching until you find user-agent as *
				line = reader.readLine();
				if(line.contains("html")){
					break;
				}
				//System.out.println(line);
			}

			if(line.equals("User-agent: *")){

				while((line = reader.readLine())!= null){
					if(line.startsWith("Disallow:")){
						String[] lineContents = line.trim().split(" ");
						if(lineContents.length > 1){
							disallowedUrlsList += lineContents[1] + ",";
						}
					}
				}
			}
		}

		
		robotStorage.putIntoRobotsDb(urlForRobot.getHost(), disallowedUrlsList);
		return disallowedUrlsList;
	}

	public static void main(String[] args) throws IOException {

		// test
		String url = "http://catalogue.bnf.fr/ark:/12148/cb119401913";
//		CustomHTTPClient cl = new CustomHTTPClient();
//		HttpResponse response = cl.getHttpResponse(url);
//		HttpEntity entity = response.getEntity();
//		ContentType contentType = null;
//		if(entity != null){
//			contentType = ContentType.get(entity);
//		}
//		System.out.println(response.getStatusLine().getStatusCode());
		CrawlPreConditionValidator pv = new CrawlPreConditionValidator();
		// true if allowed
		boolean check = pv.isAllowedToCrawl(url);
		System.out.println(check);

	}

}
