package com.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Spider.core.MainThread;

/**
 * Servlet implementation class crawlInitiatingServlet
 */
@WebServlet("/crawlInitiatingServlet")
public class crawlInitiatingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public crawlInitiatingServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String seedUrls = request.getParameter("seedUrls");
		
		String portsAndhosts = request.getParameter("hosts");
		String[] hosts = portsAndhosts.split(",");
		MainThread mainThread = new MainThread(hosts);
		mainThread.feedSeedUrls(seedUrls);
		try {
			if(request.getParameter("start") != null){
			 mainThread.beginCrawl();
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
